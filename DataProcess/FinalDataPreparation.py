#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
import random
import os
import sys
import psutil
import matplotlib
import matplotlib.pyplot as plt
import math
import pickle
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder

# # Loading some necessary files

f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/geo_dict.pkl","rb")
geo_dict = pickle.load(f)
f.close()
f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/NLP_vect_dict.pkl","rb")
NLP_vect_dict = pickle.load(f)
f.close()
f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/geo_vect_dict.pkl","rb")
geo_vect_dict = pickle.load(f)
f.close()


def create_train_set(frame_list,geomap):
    X_train = []
    y_train = []
    print ("process with length of ",len(frame_list))
    for frame in frame_list:
        training_set = frame.values
        # print(training_set)
        # print('@@@@@@@')
        #make sure there is unique geohash per frame
        #print frame.Geohash.iloc[0]
        weather_time_vector = geomap[frame.Geohash.iloc[0]]
        grid_vector = geo_dict[frame.Geohash.iloc[0]]
        try:
            NLP_vector = NLP_vect_dict[frame.Geohash.iloc[0]]
        except:
            NLP_vector = np.zeros(100)
        for i in range(8, training_set.shape[0]):#use the 8 intervals as the training data.
            if training_set[i, 4] > 0 :
                a = np.concatenate((training_set[i-8:i,4:].flatten(),weather_time_vector),axis=0)
                a = np.concatenate((a,NLP_vector),axis=0)
                a = np.append(a, grid_vector)
                X_train.append(a)
                y_train.append(1)
                
            elif random.uniform(0, 1) > 0.98:  # negative sampling for non-accident cases 
                a = np.concatenate((training_set[i-8:i,4:].flatten(),weather_time_vector),axis=0)
                a = np.concatenate((a,NLP_vector),axis=0)
                a = np.append(a, grid_vector)
                X_train.append(a)
                y_train.append(0)
    return X_train, y_train


def one_hot(train):
    Encoder = OneHotEncoder(sparse=False)
    Encoder.fit(train['HOD_cat'].values.reshape(-1, 1))

    onehot_encode = pd.concat([train.reset_index().drop('HOD_cat', 1),
                               pd.DataFrame(Encoder.transform(train['HOD_cat'].values.reshape(-1, 1)),
                                            columns=['HOD_en0', 'HOD_en1', 'HOD_en2', 'HOD_en3', 'HOD_en4'])],
                              axis=1).reindex()
    return onehot_encode.drop('index', 1)


def create_final_dataset(df,geohash_dict):
    frame_list=[]
    for idx, frame in df.groupby(df.Geohash):
        frame_list.append(frame)

    X_train, y_train = create_train_set(frame_list,geohash_dict)
    return X_train,y_train


def train_data(filename):
    df = pd.read_hdf(filename+'.h5',key='set3') # the .h5 file contains raw traffic, weather, time, and POI data
    df_normalize = df.copy()
    Accident_train = df_normalize[df_normalize.TimeStep <= df_normalize.TimeStep.max()*5/6]
    print(Accident_train.columns)
    Accident_test = df_normalize[df_normalize.TimeStep > df_normalize.TimeStep.max()*5/6]

    #Normalise the accident data content.
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaler.fit(Accident_train.loc[:,'T-Accident':])
    scaled_values = scaler.transform(Accident_train.loc[:,'T-Accident':])
    Accident_train.loc[:,'T-Accident':] = scaled_values
    scaled_values = scaler.transform(Accident_test.loc[:,'T-Accident':])
    Accident_test.loc[:,'T-Accident':] = scaled_values
    train = one_hot(Accident_train)
    # print(train.columns)
    # print('********')
    test = one_hot(Accident_test)
    X_train, y_train = create_final_dataset(train,geo_vect_dict)
    X_test, y_test = create_final_dataset(test,geo_vect_dict)

    # represent its train and test data
    np.save('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/X_train_'+filename,X_train)
    np.save('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/y_train_'+filename,y_train)
    np.save('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/X_test_'+filename,X_test)
    np.save('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/y_test_'+filename,y_test)


cities = ['Houston']

for city in cities:
    train_data(city) # creating training and test data for city