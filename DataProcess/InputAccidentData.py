#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import pytz
import pygeohash as gh
from haversine import haversine
import time
import _pickle as cPickle
import glob
import json

geohash_prec = 5  # the geo-hash level to define a region R



class weather:
    date = ''
    temp = 0.0
    windchill = 0.0
    humid = 0.0
    pressure = 0.0
    visib = 0.0
    windspeed = 0.0
    winddir = ''
    precipitation = 0.0
    events = ''
    condition = ''

    def __init__(self, date, temp, windchill, humid, pressure, visib, windspeed, winddir,
                 precipitation, events, condition, zone):
        self.date = datetime.strptime(date, '%Y-%m-%d %I:%M:%S %p')
        self.date = self.date.replace(tzinfo=pytz.timezone(zone))
        self.temp = float(temp)
        self.windchill = float(windchill)
        self.humid = float(humid)
        self.pressure = float(pressure)
        self.visib = float(visib)
        self.windspeed = float(windspeed)
        self.winddir = winddir
        self.precipitation = float(precipitation)
        self.events = events
        self.condition = condition


# #### Some meta data for each city including geo-fence and time-zone
cities = {'Houston': [29.497907,30.129003,-95.797178,-94.988191]}

time_zones = {'Houston':'US/Central'}

# time interval to sample data for
start = datetime(2017, 1, 1)
finish = datetime(2019, 12, 1)

begin = datetime.strptime('2017-01-01 00:00:00', '%Y-%m-%d %H:%M:%S')
end = datetime.strptime('2019-12-01 23:59:59', '%Y-%m-%d %H:%M:%S')

mq = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/FeatureVector/TrafficEvents_Aug16_Dec19_Publish.csv') # This is the latest version of LSTW dataset
mq.head()

mq['StartTime(UTC)'] = mq['StartTime(UTC)'].astype('datetime64[ns]', errors = 'ignore')
mq['EndTime(UTC)'] = mq['EndTime(UTC)'].astype('datetime64[ns]', errors = 'ignore')

for c in cities:
    crds = cities[c]
    subset_all = mq[(mq['StartTime(UTC)'] >= start) & (mq['StartTime(UTC)'] < end) &
                    (mq['LocationLat']>crds[0]) & (mq['LocationLat']<crds[1]) & (mq['LocationLng']>crds[2]) &
                    (mq['LocationLng']<crds[3])]

    subset_accidents = mq[(mq['Type']=='Accident') & (mq['StartTime(UTC)'] >= start) & (mq['StartTime(UTC)'] < finish)
                          & (mq['LocationLat']>crds[0]) & (mq['LocationLat']<crds[1]) & (mq['LocationLng']>crds[2])
                          & (mq['LocationLng']<crds[3])]

    print('For {} we have {} incidents, with {} accidents! ratio {:.2f}'.format(c, len(subset_all), len(subset_accidents),
                                                                               len(subset_accidents)*1.0/len(subset_all)))

    subset_accidents.to_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/MQ_{}_20170101_20191201.csv'.format(c), index=False)


## converting time from UTC to local time zone for each city
path = '/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/' #create this temporary directory inside the '/data' folder
mq_cityAccidents = {}
for c in cities:
    incidents = []
    z = time_zones[c]

    with open(path + 'MQ_{}_20170101_20191201.csv'.format(c), 'r') as file:
        header = False
        for line in file:
            if not header:
                header = True
                continue
            parts = line.replace('\r', '').replace('\n', '').split(',')
            date_start = datetime.strptime(parts[5].replace('T',' '), '%Y-%m-%d %H:%M:%S')
            date_start = date_start.replace(tzinfo=pytz.utc)
            date_start = date_start.astimezone(pytz.timezone(z))

            date_end = datetime.strptime(parts[6].replace('T',' '), '%Y-%m-%d %H:%M:%S')
            date_end = date_end.replace(tzinfo=pytz.utc)
            date_end = date_end.astimezone(pytz.timezone(z))

            v = [parts[0], parts[1], float(parts[8]), float(parts[9]), date_start, date_end]
            incidents.append(v)

    mq_cityAccidents[c] = incidents
    print ('MQ', c, len(incidents))

#Construct Feature Vectors for pairs of City-Geohash
zone_to_be = {}

for z in ['US/Central']:
    begin = begin.replace(tzinfo=pytz.utc)
    begin = begin.astimezone(pytz.timezone(z))
    end = end.replace(tzinfo=pytz.utc)
    end = end.astimezone(pytz.timezone(z))
    zone_to_be[z] = [begin, end]



# #### Get Traffic Data
def return_interval_index(time_stamp, start, end):
    if time_stamp < start or time_stamp > end:
        return -1
    index = int(((time_stamp - start).days * 24 * 60 + (time_stamp - start).seconds / 60) / 15)
    return index


diff = int(((end - begin).days * 24 * 60 + (
            end - begin).seconds / 60) / 15)
# total_minutes/15: number of 15 minutes intervals

path = '/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/'
city_to_geohashes = {}
for c in cities:
    city_to_geohashes[c] = {}

start_timestamp = time.time()
v = {}
geocode_to_airport = {}
aiport_to_timezone = {}
num = 0
test = 0
start_gh1 = []
start_gh2 = []
for c in cities:
    z = time_zones[c]

    with open(path + 'MQ_{}_20170101_20191201.csv'.format(c), 'r') as file:
        header = False
        for line in file:
            if not header:
                header = True
                continue

            parts = line.replace('\r', '').replace('\n', '').split(',')
            date_start = datetime.strptime(parts[5].replace('T', ' '), '%Y-%m-%d %H:%M:%S')
            date_start = date_start.replace(tzinfo=pytz.utc)
            date_start = date_start.astimezone(pytz.timezone(z))
            s_interval = return_interval_index(date_start, zone_to_be[z][0], zone_to_be[z][1])
            if parts[1] == 'Accident':
                num = num + 1
            if s_interval == -1:
                continue

            date_end = datetime.strptime(parts[6].replace('T', ' '), '%Y-%m-%d %H:%M:%S')
            date_end = date_end.replace(tzinfo=pytz.utc)
            date_end = date_end.astimezone(pytz.timezone(z))
            e_interval = return_interval_index(date_end, zone_to_be[z][0], zone_to_be[z][1])
            if e_interval == -1:
                e_interval = diff - 1

            start_gh = gh.encode(float(parts[8]), float(parts[9]), precision=geohash_prec)
            start_gh1.append(start_gh)
            intervals = []

            if start_gh not in city_to_geohashes[c]:
                for i in range(diff):
                    intervals.append({'Accident': 0})
            else:
                intervals = city_to_geohashes[c][start_gh]
            v = intervals[s_interval - 1]

            if parts[1] == 'Accident':
                v['Accident'] = v['Accident'] + 1
            intervals[s_interval - 1] = v
            city_to_geohashes[c][start_gh] = intervals
            ap = parts[11]
            if len(ap) > 3:
                if start_gh not in geocode_to_airport:
                    geocode_to_airport[start_gh] = set([ap])
                else:
                    st = geocode_to_airport[start_gh]
                    st.add(ap)
                    geocode_to_airport[start_gh] = st
                aiport_to_timezone[ap] = z

    for x in start_gh1:
        if x not in start_gh2:
            start_gh2.append(x)
    print(num)
    for st in range(len(start_gh2)):
        for i in range(len(city_to_geohashes[c][start_gh])):
            if city_to_geohashes[c][start_gh2[st]][i]['Accident'] > 0:
                test = test + city_to_geohashes[c][start_gh2[st]][i]['Accident']
    print(test)
    start_gh1 = []
    start_gh2 = []
    print('Done with {} in {:.1f} sec! there are {} geohashes with data!'.format(c,
                                                                                 time.time() - start_timestamp,
                                                                                 len(city_to_geohashes[c])))
    start_timestamp = time.time()
#
#Get weather data
#load and sort relevant weather data
airports_to_observations = {}
for g in geocode_to_airport:
    aps = geocode_to_airport[g]
    for a in aps:
        if a not in airports_to_observations:
            airports_to_observations[a] = []

print('{} airports to collect data for!'.format(len(airports_to_observations)))

w_path = '/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/WeatherData/'  # this directory contains weather observation records for each airport
airport_to_data = {}
for ap in airports_to_observations:
    data = []
    z = aiport_to_timezone[ap]
    print('Airport {}'.format(ap))
    header = ''
    with open(w_path + ap + '.csv', 'r') as file:
        for line in file:
            if 'Airport' in line:
                header = line.replace('\r', '').replace('\n', '').replace(',Hour', '')
                continue
            parts = line.replace('\r', '').replace('\n', '').split(',')
            try:
                w = weather(parts[1] + ' ' + parts[2].split(' ')[0] + ':00 ' + parts[2].split(' ')[1], parts[3],
                            parts[4],
                            parts[5], parts[6], parts[7], parts[8], parts[9], parts[10], parts[11], parts[12], z)
                data.append(w)
            except:
                continue
    data.sort(key=lambda x: x.date)
    airport_to_data[ap] = data
print('\nData for {} airport stations is loaded!'.format(len(airport_to_data)))


# Find missing Airports
for c in city_to_geohashes:
    for g in city_to_geohashes[c]:
        if g not in geocode_to_airport:
            print(1)
            gc = gh.decode_exactly(g)[0:2]
            min_dist = 1000000000
            close_g = ''
            for _g in geocode_to_airport:
                _gc = gh.decode_exactly(_g)[0:2]
                dst = haversine(gc, _gc, 'km')
                if dst < min_dist:
                    min_dist = dst
                    close_g = _g
            geocode_to_airport[g] = geocode_to_airport[close_g]

city_to_geohashes_to_weather = {}
for c in city_to_geohashes:
    start = time.time()
    geo2weather = {}
    for g in city_to_geohashes[c]:
        w_data = []
        for i in range(len(city_to_geohashes[c][g])):
            w_data.append({'Temperature': [], 'Humidity': [], 'Pressure': [], 'Visibility': [], 'WindSpeed': [],
                           'Precipitation': [], 'Condition': set(), 'Event': set()})
        # populate weather data
        aps = geocode_to_airport[g]
        for a in aps:
            z = aiport_to_timezone[a]
            a_w_data = airport_to_data[a]
            prev = 0
            for a_w_d in a_w_data:
                idx = return_interval_index(a_w_d.date, zone_to_be[z][0], zone_to_be[z][1])
                if idx > -1:
                    for i in range(prev, min(idx + 1, len(w_data))):
                        w1 = w_data[i]
                        tmp1 = w1['Temperature']
                        if a_w_d.temp > -1000:
                            tmp1.append(a_w_d.temp)
                            w1['Temperature'] = tmp1
                        hmd1 = w1['Humidity']
                        if a_w_d.humid > -1000:
                            hmd1.append(a_w_d.humid)
                            w1['Humidity'] = hmd1
                        prs1 = w1['Pressure']
                        if a_w_d.pressure > -1000:
                            prs1.append(a_w_d.pressure)
                            w1['Pressure'] = prs1
                        vis1 = w1['Visibility']
                        if a_w_d.visib > -1000:
                            vis1.append(a_w_d.visib)
                            w1['Visibility'] = vis1
                        wspd1 = w1['WindSpeed']
                        if a_w_d.windspeed > -1000:
                            wspd1.append(a_w_d.windspeed)
                            w1['WindSpeed'] = wspd1
                        precip1 = w1['Precipitation']
                        if a_w_d.precipitation > -1000:
                            precip1.append(a_w_d.precipitation)
                            w1['Precipitation'] = precip1

                        cond1 = w1['Condition']
                        cond1.add(a_w_d.condition)
                        w1['Condition'] = cond1

                        evnt1 = w1['Event']
                        evnt1.add(a_w_d.events)
                        w1['Event'] = evnt1

                        w_data[i] = w1

                    prev = idx + 1

        geo2weather[g] = w_data
    city_to_geohashes_to_weather[c] = geo2weather
    print('Done with {} in {:.1f} sec!'.format(c, time.time() - start))

#map each time-step to hour of day and day of the week;
timestep_to_dow_hod = {}
d_begin = begin.replace(tzinfo=pytz.utc)
d_end = end.replace(tzinfo=pytz.utc)
index = 0

while (d_begin < d_end):
    dow = d_begin.weekday()
    hod = d_begin.hour
    timestep_to_dow_hod[index] = [dow, hod]

    d_begin += timedelta(seconds=15 * 60)
    index += 1

traffic_tags = ['Accident']
weather_tags = ['Condition', 'Event', 'Humidity', 'Precipitation', 'Pressure', 'Temperature', 'Visibility', 'WindSpeed']
poi_tags = []
start = time.time()
condition_tags = set()

for c in city_to_geohashes:
    # creating vector for each reion (geohash) during a 15 minutes time interval. Such vector contains time, traffic, and weather attributes.
    writer = open('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentVectors/{}_geo2vec_{}-{}.csv'.format(c, str(
        begin.year) + str(begin.month) + str(begin.day),str(end.year) + str(end.month) + str(end.day)),'w')

    writer.write('Geohash,TimeStep,DOW,HOD,T-Accident,W-Humidity,W-Precipitation,W-Pressure,'
                 'W-Temperature,W-Visibility,W-WindSpeed,W-Rain,W-Snow,W-Fog,W-Hail\n')

    traffic = city_to_geohashes[c]
    weather = city_to_geohashes_to_weather[c]
    for g in traffic:
        vectors = []
        for i in range(len(traffic[g])):
            v = []
            for t in traffic_tags:
                v.append(traffic[g][i][t])
            v_w = [0, 0, 0, 0]  # for rain, snow, fog, and hail
            for w in weather_tags:
                if w == 'Condition' or w == 'Event':
                    _tgs = weather[g][i][w]
                    for _tg in _tgs:
                        if 'rain' in _tg.lower() or 'drizzle' in _tg.lower() or 'thunderstorm' in _tg.lower():
                            v_w[0] = 1
                        elif 'snow' in _tg.lower():
                            v_w[1] = 1
                        elif 'fog' in _tg.lower() or 'haze' in _tg.lower() or 'mist' in _tg.lower() or 'smoke' in _tg.lower():
                            v_w[2] = 1
                        elif 'hail' in _tg.lower() or 'ice pellets' in _tg.lower():
                            v_w[3] = 1
                elif len(weather[g][i][w]) == 0:
                    v.append(0)
                else:
                    v.append(np.mean(weather[g][i][w]))
            for _v_w in v_w:
                v.append(_v_w)
            vectors.append(v)

        for i in range(len(vectors)):
            v = vectors[i]
            v = [str(v[j]) for j in range(len(v))]
            v = ','.join(v)
            writer.write(g + ',' + str(i) + ',' + str(timestep_to_dow_hod[i][0]) + ',' + str(timestep_to_dow_hod[i][1])
                         + ',' + v + '\n')

    writer.close()