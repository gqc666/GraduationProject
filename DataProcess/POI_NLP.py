#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from datetime import datetime, timedelta
import pytz
import pygeohash as gh
from haversine import haversine
import time
import _pickle as cPickle
import glob
import json
import re

geohash_prec = 5

cities = {'Houston': [29.497907,30.129003,-95.797178,-94.988191]}

time_zones = {'Houston':'US/Central'}

# A time interval of length 1 year, to be used to generate description to vector for each geographical region (or geohash)
start = datetime(2016, 8, 2)
finish = datetime(2017, 8, 1)

begin = datetime.strptime('2016-08-02 00:00:00', '%Y-%m-%d %H:%M:%S')
end = datetime.strptime('2017-08-01 23:59:59', '%Y-%m-%d %H:%M:%S')

#Load Past Traffic Events Data
mq = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/FeatureVector/TrafficEvents_Aug16_Dec19_Publish.csv') # this is the latest version of LSTW dataset
mq.head()


mq['StartTime(UTC)'] = mq['StartTime(UTC)'].astype('datetime64[ns]', errors = 'ignore')
mq['EndTime(UTC)'] = mq['EndTime(UTC)'].astype('datetime64[ns]', errors = 'ignore')

for c in cities:
    crds = cities[c]
    subset_accidents = mq[(mq['Type']=='Accident') & (mq['StartTime(UTC)'] >= start) & (mq['StartTime(UTC)'] < finish)
                          & (mq['LocationLat']>crds[0]) & (mq['LocationLat']<crds[1]) & (mq['LocationLng']>crds[2])
                          & (mq['LocationLng']<crds[3])]

    subset_accidents.to_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/DataForNLP/MQ_{}_20160802_20170801.csv'.format(c), index=False)


#Load GloVe Word Embedding Vectors dictionary
word2vec = {}
v1 = []
for i in range(100):
    v1.append(i)
v1 = np.mean(v1, axis=0)
with open('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/FeatureVector/glove.6B.100d.txt','r') as reader:  # suppose that we already downloaded this GloVe model.
    for line in reader:
        parts = line.replace('\r', '').replace('\n', '').split(' ')
        v = [float(parts[i]) for i in range(1, len(parts))]
        word2vec[parts[0]] = v
print('loaded {} word vectors!'.format(len(word2vec)))


#Return the natural language description of the accident event.
def return_desc2vec(input):
    nlp = []
    parts = re.split(' - | |\.|\\\|/|;|,|&|!|\?|\(|\)|\[|\]|\{|\}', input)
    parts = [p.lower() for p in parts]
    for p in parts:
        if len(p) == 0:
            continue
        if p in word2vec:
            nlp.append(word2vec[p])
    if len(nlp) == 0:
        print(input)
    nlp = np.mean(nlp, axis=0)
    return nlp

#Use Traffic Event Data to Create Embedding Vector
# load valid geohashes
valid_geohashes = set()  #Only generate data for those regions/geohashes that have valid POI data
with open('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/FeatureVector/geohash_to_poi_vec.csv', 'r') as reader:
    for line in reader:
        if 'Geohash' in line:
            continue
        valid_geohashes.add(line.split(',')[0])

geo_to_vec = {}
start_timestamp = time.time()

for c in cities:
    with open('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/DataForNLP/MQ_{}_20160802_20170801.csv'.format(c),'r') as file:
        header = False
        for line in file:
            if not header:
                header = True
                continue
            parts = line.replace('\r', '').replace('\n', '').split(',')

            accident_gh = gh.encode(float(parts[8]), float(parts[9]), precision=geohash_prec)
            if accident_gh not in valid_geohashes:
                continue
            temp_grid = []
            if accident_gh in geo_vectors:
                temp_grid = geo_vectors[accident_gh]
            temp_grid.append(return_desc2vec(parts[4]))
            geo_vectors[start_gh] = temp_grid

    print('Done with {} in {:.1f} sec!'.format(c, time.time() - start_timestamp))
    start_timestamp = time.time()

#Create the natural language description for each geohash/grid

writer = open('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/DataForNLP/geohash_to_text_vec.csv', 'w')
writer.write('Geohash,vec\n')

for g in geo_vectors:
    vec = list(np.mean(geo_vectors[g], axis=0))
    v = [str(vec[i]) for i in range(len(vec))]
    v = ' '.join(v)
    writer.write(g + ',' + v + '\n')
writer.close()