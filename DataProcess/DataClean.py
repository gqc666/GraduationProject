#!/usr/bin/env python
# coding: utf-8

# Preparing for Integration
import pandas as pd
import numpy as np
import random
import os
import sys
import psutil
import matplotlib
import matplotlib.pyplot as plt
import math
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import MinMaxScaler
import pickle

#Refine POI Vector for each grid
geohash_map = pd.read_csv("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/FeatureVector/geohash_to_poi_vec.csv")

geohash_vec = geohash_map[[u'Amenity', u'Bump', u'Crossing', u'Give_Way',
                           u'Junction', u'Noexit', u'Railway', u'Roundabout', u'Station', u'Stop',
                           u'Traffic_Calming', u'Traffic_Signal', u'Turning_Circle',
                           u'Turning_Loop']]
#Normalise the POI data
scaler = MinMaxScaler(feature_range=(0, 1))
scaler.fit(geohash_vec.loc[:, 'Amenity':])
scaled_values = scaler.transform(geohash_vec.loc[:, 'Amenity':])
geohash_vec.loc[:, 'Amenity':] = scaled_values

geohash_dict = {}
for index, row in geohash_map.iterrows():
    geohash_dict[row.Geohash] = np.array(geohash_vec.iloc[index])

#Store the data by the format of pickle
f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/geo_vect_dict.pkl", "wb")
pickle.dump(geohash_dict, f)
f.close()

geo_dict = dict(zip(geohash_map.Geohash.unique(), range(len(geohash_map.Geohash.unique()))))
# for i in range(len(geohash_map.Geohash.unique())):
#     if geohash_map.Geohash.unique()[i] == '9vhpz':
#         print(88888888888888888888)

print('------------')
f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/geo_dict.pkl", "wb")
pickle.dump(geo_dict, f)
f.close()

#Refine Description2Vector data for each Geohash (or geographical region)
NLP_map = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/DataForNLP/geohash_to_text_vec.csv')
NLP_dict = {}
for index, row in NLP_map.iterrows():
    NLP_dict[row.Geohash] = np.array([float(x) for x in row.vec.split(' ')])
f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/NLP_vect_dict.pkl", "wb")
pickle.dump(NLP_dict, f)
f.close()

#Clean the data by Adding grid Code
def clean_data(filepath, storename):
    df = pd.read_csv(filepath)
    list_ = df.columns
    print(list_)
    temp1 = df['Geohash'].unique()
    temp_df = df[[u'TimeStep', u'T-Accident', u'Geohash', u'HOD', u'DOW', u'W-Humidity',
                  u'W-Precipitation', u'W-Pressure', u'W-Temperature', u'W-Visibility',
                  u'W-WindSpeed', u'W-Rain', u'W-Snow', u'W-Fog', u'W-Hail']]
    temp_df.to_hdf(storename + '.h5', key='set1')

    f = open("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/Dict/geo_dict.pkl", "rb")
    geo_dict = pickle.load(f)
    print(len(temp1))
    for i in range(len(temp1)):
        num = 0
        for key in geo_dict :
            if temp1[i] == key:
                num+=1
            else:
                num+=0
        if num==0:
            df = df[~df['Geohash'].isin([temp1[i]])]

    f.close()
    df['geohash_code'] = df.apply(lambda row: geo_dict[row['Geohash']], axis=1)
    temp_df = df[[u'TimeStep', u'T-Accident', u'Geohash', u'geohash_code', u'HOD', u'DOW', u'W-Humidity',
                  u'W-Precipitation', u'W-Pressure', u'W-Temperature', u'W-Visibility',
                  u'W-WindSpeed', u'W-Rain', u'W-Snow', u'W-Fog', u'W-Hail']]
    temp_df.to_hdf(storename + '.h5', key='set2')
    df = pd.read_hdf(storename + '.h5', key='set2')

    # Modify the data type of week
    def day_of_week(DOW):
        if DOW < 5:
            return 1
        else:
            return 0

    def shift(group):
        df_list = []
        for idx, df in group:
            df_list.append(df)
        return pd.concat(df_list)
    #Modify the data type of hour
    def hour_of_day(HOD):
        if HOD >= 6 and HOD < 10:
            return 0
        if HOD >= 10 and HOD < 15:
            return 1
        if HOD >= 15 and HOD < 18:
            return 2
        if HOD >= 18 and HOD < 22:
            return 3
        else:
            return 4

    # 1 means that accident happened, 0 means that accident didn't happen
    def accident_happen(d):
        if d > 0:
            return 1
        else:
            return 0

    df['DOW_cat'] = df.apply(lambda row: day_of_week(row['DOW']), axis=1)
    df['HOD_cat'] = df.apply(lambda row: hour_of_day(row['HOD']), axis=1)
    df['T-Accident'] = df.apply(lambda row: accident_happen(row['T-Accident']), axis=1)
    group = df.groupby('Geohash')
    df = shift(group)
    temp_df = df[
        [u'TimeStep', u'Geohash', u'geohash_code', u'HOD_cat', u'DOW_cat', u'T-Accident', u'W-Humidity',
         u'W-Precipitation', u'W-Pressure', u'W-Temperature', u'W-Visibility',u'W-WindSpeed',
         u'W-Rain', u'W-Snow', u'W-Fog', u'W-Hail']]
    temp_df.to_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/temp_df_{}.csv'.format(city),
                   index=False)
    temp_df.to_hdf(storename + '.h5', key='set3')

cities = ['Houston']

for city in cities:
    clean_data("/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentVectors/{}_geo2vec_20161231-2019121.csv".format(city),
               city)