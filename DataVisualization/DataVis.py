import pandas as pd
import numpy as np
mq = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/temp_df_Houston.csv')
#print(mq)
temp = mq['geohash_code'].drop_duplicates(keep='first')
#print(temp)
geohash = []
temp = temp.values.tolist()
for i in range(len(temp)):
    geohash.append(temp[i])

#print(geohash)
geohash_num_acc = dict(name = geohash)
for i in range(len(geohash)):
    num = 0
    for j in range(len(mq)):
        if mq['geohash_code'][j] == geohash[i] and mq['T-Accident'][j] > 0:
            num += mq['T-Accident'][j]
    geohash_num_acc[geohash[i]] = num
    print(geohash_num_acc[geohash[i]])