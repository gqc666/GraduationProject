# # import folium
# # import pandas as pd
# #
# # # define the world map
# # world_map = folium.Map()
# #
# # # display world map
# # world_map
#
# import  folium
# import webbrowser
# from importData import latitude_data, longitude_data, length_la
# m = folium.Map(location=[40.7606, -73.96434],
#               zoom_start=14)
#
# for i in range(1000):
#     folium.Marker([latitude_data[i],longitude_data[i]], icon=folium.Icon(color='red')).add_to(m)  # 定一个点，放到地图m上
#
# # folium.Marker([40.22,116.72],popup='<b>浮标上面的那个文字</b>',icon=folium.Icon(color='red')).add_to(m)
# # # 把浮标变成红色
# # folium.Marker([40.24,116.74],popup='<b>浮标上面的那个文字</b>',icon=folium.Icon(color='green',icon='info-sign')).add_to(m)
# # # 浮标改图样
# #
# # #标记一个空心的圈
# # folium.Circle(
# #     location=[40.2,117.7],
# #     radius=10000,
# #     color='crimson',
# #     popup='popup',
# #     fill=False
# # ).add_to(m)
# #
# # #标记一个实心圆
# # folium.CircleMarker(
# #     location=[39.2,117.7],
# #     radius=100,
# #     popup='popup',
# #     color='#DC143C',#圈的颜色
# #     fill=True,
# #     fill_color='#6495ED' #填充颜色
# # ).add_to(m)
#
# m.save('map.html')
# webbrowser.open('map.html')


# import numpy as np
# import pandas as pd
# import seaborn as sns
# import folium
# import webbrowser
# from folium.plugins import HeatMap
# from importData import latitude_data, longitude_data, length_la
# #导入数据集：
# # posi = pd.read_excel("D:/Python/File/Cities2015.xlsx")
# # posi = posi.dropna()
#
# #生成所需要的数组格式数据：
# # lat = np.array(posi["lat"][0:len(posi)])
# # lon = np.array(posi["lon"][0:len(posi)])
# # pop = np.array(posi["pop"][0:len(posi)],dtype=float)
# # gdp = np.array(posi["GDP"][0:len(posi)],dtype=float)
# data1 = [[latitude_data[i],longitude_data[i]] for i in range(length_la)]
#
# #创建以高德地图为底图的密度图：
# map_osm = folium.Map(
#     location=[40.7606, -73.96434],
#     zoom_start=14,
#     # tiles='http://webrd02.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}',
#     # attr="&copy; <a href=\"http://ditu.amap.com/\">高德地图</a>"
#     )
# #创建以腾讯地图为底图的密度图：
# # map_osm = folium.Map(
# #     location=[35,110],
# #     zoom_start=5,
# #     tiles='http://rt{s}.map.gtimg.com/realtimerender?z={z}&x={x}&y={y}&type=vector&style=0',
# #     attr="&copy; <a href=\"http://map.qq.com/\">腾讯地图</a>"
# #     )
# #生成交互式地图：
# HeatMap(data1).add_to(map_osm)
# #file_path = r"D:/Python/Image/People.html"
# map_osm.save('heat.html')
# webbrowser.open('heat.html')


import pandas as pd
import numpy as np
import os
import folium
from folium import plugins
import webbrowser
import geopandas as gp
import matplotlib.pyplot as plt
# from importData import latitude_data, longitude_data, length_la

#数据导入：
# full = pd.read_excel("D:/Python/File/Cities2015.xlsx")
# full = full.dropna()

plot_map = folium.Map(location=[29.497907, -95.797178], zoom_start=14)
mq = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/MQ_Houston_201806_201809.csv')
for i in range(len(mq)):
    folium.Circle(radius=30, location=(mq['LocationLat'][i],mq['LocationLng'][i]), color='red', fill=True, fill_color='black').add_to(plot_map)
plot_map.save('plot_map.html')
webbrowser.open('plot_map.html')

mq = pd.read_csv('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/AccidentData/temp_df_Houston.csv')
num_noneweek = 0
num_week = 0
names = []
names.append('num_noneweek')
names.append('num_week')
print(len(mq))
for i in range(len(mq)):
    if mq['DOW_cat'][i] == 0:
        num_noneweek +=1
    else:
        num_week+=1

dict1 = dict(name = names)
dict1['num_noneweek'] = num_noneweek
dict1['num_week'] = num_week

plt.style.use('ggplot')
name_list = ['Not in weekdays', 'In weekdays']
num_list = [num_noneweek,num_week]
rects = plt.bar([0.5,1.0], num_list, color='steelblue',width = 0.1)
# X轴标题
index = [0.5, 1]
# index = [float(c) + 0.1 for c in index]
plt.ylim(ymax=1500000, ymin=0)
plt.xticks(index,name_list)
plt.ylabel("Number of accidents")  # X轴标签
for rect in rects:
    height = rect.get_height()
    plt.text(rect.get_x() + rect.get_width()/2, height, str(height), ha='center', va='bottom')
plt.show()

