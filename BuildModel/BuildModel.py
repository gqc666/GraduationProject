import os
import sys
import psutil

import pandas as pd
import numpy as np

import keras
import random

from keras.models import Sequential, Model
from keras.layers import Dense, LSTM, Activation, Dropout, BatchNormalization, Input, Embedding
from keras.layers import Flatten, Conv2D, MaxPooling2D, Bidirectional, concatenate

from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from keras import regularizers
from keras import optimizers
import keras.metrics as km

import matplotlib
import matplotlib.pyplot as plt
import math
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

from keras.utils import np_utils
from sklearn.metrics import recall_score, precision_score, f1_score, accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report
from sklearn.metrics import mean_absolute_error, mean_squared_error
from sklearn.metrics import make_scorer
from keras.wrappers.scikit_learn import KerasClassifier

from sklearn.metrics import roc_curve, auc

from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle

from keras_self_attention import SeqSelfAttention
from base import *

verbose = 2
dropout = 0.2
VAL_SPLIT = 0.2
patience = 15
lr = 0.01
weight_decay = 0.0000
lr_decay = 1e-6
ADD_ON_LAYERS = True
ACT_PRIOR = 'sigmoid'
ACT_POSTERIOR = 'relu'
LSTM_UNIT = 128
GEOHASH_UNIT = 128
EMBEDDING_UNIT = 128
Embedding_outdim = 128
NLP_UNIT = 128
SEQ_UNIT = 256
DENSE_CONCAT = 512
CONV_UNIT = 32
weights = np.array([1, 1])

def show_results(nn_model_train): # plot performance over the training epochs
    accuracy     = nn_model_train.history['accuracy']
    val_accuracy = nn_model_train.history['val_accuracy']
    loss         = nn_model_train.history['loss']
    val_loss     = nn_model_train.history['val_loss']
    epochs       = range(len(accuracy))
    nb_epochs    = len(epochs)

    f2 = plt.figure(2)
    plt.subplot(1,2,1)
    plt.axis((0,nb_epochs,0,1.2))
    plt.plot(epochs, accuracy, label='Training accuracy',color='red',linewidth=2.0,linestyle='--')
    plt.plot(epochs, val_accuracy, label='Validation accuracy',color='blue',linewidth=3.0,linestyle='-.')
    plt.title('Training and validation accuracy')
    plt.legend()
    plt.subplot(1,2,2)
    plt.axis((0,nb_epochs,0,1.2))
    plt.plot(epochs, loss, 'bo', label='Training loss')
    plt.plot(epochs, val_loss, 'b', label='Validation loss')
    plt.title('Training and validation loss')
    plt.legend()
    plt.draw()
    plt.pause(10)

class Traffic_Model(keras_model):
    def load_data(self):
        super(Traffic_Model, self).load_data()

        self.X_train1 = self.reshape(self.X_train)# The Weather/Time attributes
        self.X_test1 = self.reshape(self.X_test)

        self.X_train2 = reshape_cat(self.X_train, 'geohash')  # geohash indicates POI attributes
        self.X_train3 = reshape_cat(self.X_train, 'NLP')  # NLP indicates Natural Language Description attributes

        self.X_test2 = reshape_cat(self.X_test, 'geohash')
        self.X_test3 = reshape_cat(self.X_test, 'NLP')
    def create_model(self):
        # **************Weather/Time data***************************
        input1 = Input(shape=(self.X_train1.shape[1], self.X_train1.shape[2]), dtype='float32',
                       name='main_input')
        lstm = LSTM(units=LSTM_UNIT, return_sequences=True,
                    kernel_regularizer=regularizers.l2(self.weight_decay),
                    recurrent_regularizer=regularizers.l2(self.weight_decay),
                    dropout=dropout,
                    recurrent_dropout=dropout,
                    unroll=True)(input1)

        lstm = LSTM(units=LSTM_UNIT, return_sequences=False,
                    kernel_regularizer=regularizers.l2(self.weight_decay),
                    recurrent_regularizer=regularizers.l2(self.weight_decay),
                    dropout=dropout,
                    recurrent_dropout=dropout,
                    unroll=True)(lstm)
        #**************POI data***************************
        input2 = Input(shape=(self.X_train2.shape[1],), dtype='float32', name='geohash_input')
        geohash_vec = Dense(GEOHASH_UNIT, activation=ACT_PRIOR)(input2)
        #**************Natural Language Description data***************************
        input3 = Input(shape=(self.X_train3.shape[1],), dtype='float32', name='nlp_input')
        nlp_vec = Dense(NLP_UNIT, activation=ACT_PRIOR)(input3)
        #**************n***************************
        level_2 = concatenate([lstm, geohash_vec, nlp_vec])

        main_output = self.last_layers(level_2)

        self.model = Model(inputs=[input1, input2, input3], outputs=main_output)

        print(self.model.summary())

    def train(self):#Train the model
        history = self.model.fit([self.X_train1, self.X_train2, self.X_train3], self.y_train,
                                 batch_size=self.batch_size,
                                 epochs=self.epoch, verbose=verbose, validation_split=VAL_SPLIT)
        show_results(history)
        return history

    def evaluate(self):
        y_true, y_pred = self.y_test, self.model.predict([self.X_test1, self.X_test2, self.X_test3], verbose=verbose)
        mse_predict = mean_squared_error(y_true, y_pred)
        print("MSE is: ")
        print(mse_predict)
        y = (mse_predict**0.5)
        print("RMSE is: ")
        print(y)
        mae_predict = mean_absolute_error(y_true, y_pred)
        print("MAE is: ")
        print(mae_predict)
        return self.make_report(y_true, y_pred)

def Train_Model(city='Houston'):
    def initialteModel():
        mymodel = Traffic_Model(city=city)
        return mymodel

    def buildModel(mymodel):
        mymodel.load_data()#Load data to the model
        mymodel.create_model()
        mymodel.compile_model()
        mymodel.train()
        return pred

    def runModel(classname):
        print("*" * 20, classname, "*" * 20)
        mymodel = initialteModel()
        mymodel = buildModel(mymodel)
        result = mymodel.evaluate()
        return result

    return runModel('Traffic_Accident_prediction_Model')


cities = ['Houston']
for city in cities:
    result = Train_Model(city)