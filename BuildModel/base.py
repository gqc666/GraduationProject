import os
import sys
import psutil

import pandas as pd
import numpy as np

import keras
import random

from keras.models import Sequential,Model
from keras.layers import Dense,LSTM,Activation,Dropout,BatchNormalization,Input,Embedding
from keras.layers import Flatten,Conv2D,MaxPooling2D,Bidirectional,concatenate

from keras.utils import to_categorical
from keras.callbacks import EarlyStopping
from keras import regularizers
from keras import optimizers
import keras.metrics as km

import matplotlib
import matplotlib.pyplot as plt
import math

from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler

from keras.utils import np_utils
from sklearn.metrics import recall_score,precision_score,f1_score,accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import classification_report

from sklearn.metrics import make_scorer
from keras.wrappers.scikit_learn import KerasClassifier


from sklearn.metrics import roc_curve, auc

from scipy import interp
import matplotlib.pyplot as plt
from itertools import cycle

from keras_self_attention import SeqSelfAttention

import seaborn as sns

SEQ=8 #sequence for LSTM

verbose = 2
dropout=0.4
VAL_SPLIT = 0.2
patience = 15
lr=0.001
weight_decay = 0.0001
lr_decay=1e-4
ADD_ON_LAYERS = True
ACT_PRIOR = 'sigmoid'
ACT_POSTERIOR = 'relu'
LSTM_UNIT = 256
GEOHASH_UNIT = 128
EMBEDDING_UNIT = 128
Embedding_outdim = 128
NLP_UNIT = 128
SEQ_UNIT = 256
DENSE_CONCAT = 256
CONV_UNIT = 32
weights = np.array([1,1])

class base_model(object):        
    def __init__(self,n_jobs=10,act=ACT_POSTERIOR,city='Houston'):
        self.n_jobs=n_jobs
        self.CITY=city
        self.act=act
   
    def load_data(self,category=None,with_geocode=False):
        self.X_train = np.load('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/X_train_'+self.CITY+'.npy')
        self.y_train = np.load('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/y_train_'+self.CITY+'.npy')
        self.X_test = np.load('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/X_test_'+self.CITY+'.npy')
        self.y_test = np.load('/Users/guoquancheng/Desktop/DAP_project/venv/GQC_Project/Data/TrainAndTest/y_test_'+self.CITY+'.npy')
        if not with_geocode:
            self.X_train = self.X_train[:,0:-1]    
            self.X_test = self.X_test[:,0:-1]     
        self.update_y()
        if category!=None:
            l_train=[]
            l_test=[]
            for cat in category:
                l_train.append(reshape_cat(self.X_train,cat))
                l_test.append(reshape_cat(self.X_test,cat))
            self.X_train = np.concatenate(l_train,axis=1)
            self.X_test = np.concatenate(l_test,axis=1)

    def update_y(self):
        self.y_train = to_categorical(self.y_train,2)
        self.y_test = to_categorical(self.y_test,2)
    def last_layers(self,layer):
        # ******************layer1**********************
        layer = Dense(DENSE_CONCAT,
                    kernel_regularizer=regularizers.l2(self.weight_decay),
                    activation=self.act)(layer)
        #******************layer2**********************
        layer = Dense(units=int(DENSE_CONCAT/2),
                    kernel_regularizer=regularizers.l2(self.weight_decay),
                    activation=None)(layer)
        layer = Dropout(dropout)(layer)
        layer = BatchNormalization()(layer)
        layer = Activation(self.act)(layer)
        layer = Dropout(dropout)(layer)
        # ******************layer3**********************
        layer = Dense(units=int(DENSE_CONCAT/8),
                    kernel_regularizer=regularizers.l2(self.weight_decay),
                    activation=None)(layer)
        # ******************Output layer**********************
        main_output = Dense(self.output_dim, activation=self.activation)(layer)
        return main_output
    

class keras_model(base_model):
    def __init__(self,city='Houston',activation='softmax',batch_size=64,epoch = 1,n_jobs=1,act=ACT_POSTERIOR):
        super(keras_model,self).__init__(act=act,city=city)
        self.output_dim = 2
        self.activation=activation
        self.batch_size = batch_size
        self.epoch = epoch
        self.n_jobs=n_jobs
        self.weight_decay = weight_decay
        self.lr=lr
        self.lr_decay=lr_decay
        
    def reshape(self,x):#Reshape the Weather and Time data
        x = x[:,0:-114]
        x = x.reshape((x.shape[0], SEQ, int(x.shape[1]/SEQ)))
        return x
    
    def compile_model(self,model=None):
        opt = optimizers.SGD(lr=0.0001)
        self.model.compile(loss="mean_absolute_error", optimizer=opt,metrics=['accuracy'])
    def create_model(self):
        self.model = KerasClassifier(build_fn=self.build_model, epochs=self.epoch, batch_size=self.batch_size, verbose=1)
    def make_report(self,y_true,y_pred):
        data_frame = classification_report(y_true.argmax(axis=-1), y_pred.argmax(axis=-1))
        print(data_frame)
        

def reshape_cat(array, category):#Reshape the natural language description and POI attributes
    l = []
    b = array[:, 0:-14]
    if category != 'geohash' and category != 'NLP':
        for i in range(SEQ):
            c = b[:, i * 25:i * 25 + 25]
            if category == 'traffic':
                d = np.concatenate([c[:, 1:2], c[:, 3:10]], axis=1)
            elif category == 'weather':
                d = c[:, 10:-5]
            elif category == 'time':
                d = np.concatenate([c[:, 0:1], c[:, 2:3], c[:, -5:]], axis=1)
            else:
                d = c
            l.append(d)
        n = np.concatenate(l, axis=1)
        return n
    elif category == 'NLP':
        return array[:, -100:]
    else:
        return array[:, -114:-100]